﻿namespace websharper_CRUD

open WebSharper
open WebSharper.UI
open WebSharper.UI.Templating
open WebSharper.UI.Notation
open WebSharper.UI.Html
open WebSharper.UI.Client

open System


[<JavaScript>]
module Templates =

    type MainTemplate = Templating.Template<"Main.html", ClientLoad.FromDocument, ServerLoad.WhenChanged>

[<JavaScript>]
module Client =

    // Helper
    let classes (l:string list) = attr.``class`` (String.concat " " l )

    let Main () =
      async {

        // Get the users from the server
        let! users = Server.GetUsers()
        // Create a ListModel, using the user's id as identifier.
        // We initialise the ListModel with an empty list
        // Notice this is totally independent from the retrieval of users from the server
        let usersCollection =
            ListModel.Create
                (fun (u:DataStore.User) -> u.id)
                []
        // A Var holding some feedback to the user
        let resultVar = Var.Create Doc.Empty
        // If the editedUser Var holds Some user, we will display the edition for for that user
        let editedUser:Var<Option<DataStore.UserSpec>> = Var.Create None
        //
        // Handle the users retrieved from the server
        // and update the list model accordingly.
        let res =
            match users with
            // If we successful, update the ListModel
            | Ok l ->
                usersCollection.Set l
                resultVar.Set Doc.Empty
            // In case of error, empty the ListModel and notify user
            | Error es ->
                usersCollection.Set [||]
                resultVar.Set(
                  div [] [text "Error retrieving users"]
                )


        // Map the ListModel to a table.
        let usersTableView =
          usersCollection.View
          |> View.Map
              // l is the Seq wrapped by the ListModel, hence our users
              (fun l ->
                table
                    [ attr.``class`` "table table-bordered table-striped table-hover table-sm"; attr.id "admin_users_table" ]
                    [
                        thead []
                              [
                                tr []
                                   [
                                        th []
                                           [
                                                div []  [text "Name"]
                                           ]
                                        th []
                                           [
                                                div []  [text "Edit"]
                                           ]

                                   ]

                                ]

                        tbody []
                              [
                                    l
                                    |> Seq.map (fun u ->
                                        tr
                                            []
                                            [
                                                td [attr.``class`` "table-sm"] [text u.name]
                                                td [attr.``class`` "table-sm"; on.click (fun _el _ev -> editedUser.Set (u.toSpec()|> Some))] [text "Edit"]
                                            ]
                                    )
                                    |> Doc.Concat
                                ]

                    ]
              )

        // This function returns the user form.
        // It handles both edit and creations, depending of the value in the id field (resp Some and None)
        let editionFormForUser (user:DataStore.UserSpec) =
            // Put the user in a var and create lenses to easily link it to form fields
            let u = Var.Create user
            // Create a lens on the name, giving us a Var we can use in the form field
            let name =
                u.LensAuto (fun u -> u.name)
            // Return the form Doc
            form
                [ attr.id "admin_users_form"]
                [
                    // The name field and its label
                    label [attr.``for`` "userEditionName"] [text "Name"]
                    Doc.InputType.Text [attr.id "userEditionName"] name
                    // Submid button
                    Doc.Button
                        "Save"
                        [attr.``class`` "btn btn-primary"]
                        // This is handling the click
                        (fun () ->
                           async {
                                    // Call the server side
                                    let! r =Server.addOrEditUser u.Value
                                    match r with
                                    | Ok u ->
                                        // We get the updated user back, inject it in the users collection used to display the table
                                        // Add updates an existing entry
                                        usersCollection.Add u
                                        // Notify of the success
                                        resultVar.Set (div [] [text "UpdateSuccessful"])
                                        // Get out of user edition, hiding the form
                                        editedUser.Set None
                                    // This Ok l should not happen as single row  result is ensured server side
                                    | _ ->
                                      resultVar.Set (div [] [text "An error occured"])
                            }
                            |> Async.Start
                        )
                    Doc.Button
                        "Cancel"
                        [attr.``class`` "btn btn-secondary"]
                        // Cancelling is simply going to a state where no user edition is done
                        (fun () -> editedUser.Set None)
                ]
        // The view displaying the for or an empty doc according to the editedUser Var value.
        // We place this view in the doc we return below
        let editionformView () =
            editedUser.View
            |> View.Map (fun uo ->
                match uo with
                | None -> Doc.Empty
                | Some u -> editionFormForUser u
            )

        // Button to add a user, displaying the empty form
        let addButton =
            Doc.Button
                "Add new"
                [attr.``class`` "btn btn-primary"]
                (fun () ->
                    resultVar.Set Doc.Empty
                    // Set the value in editedUser to an "empty" record with
                    // its id field set to None indicating it is not yet stored in the database
                    editedUser.Set (Some(DataStore.UserSpec.Init())))

        // Finally, return the Doc, inserting the view of the table
        return
          div
            []
            [
              resultVar.V
              editionformView().V
              addButton
              usersTableView.V
            ]

      }
      // Map our Async<Doc> to a Doc
      |>Client.Doc.Async
